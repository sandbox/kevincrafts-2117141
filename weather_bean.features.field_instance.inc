<?php
/**
 * @file
 * weather_bean.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function weather_bean_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'bean-weather-field_weather_city'
  $field_instances['bean-weather-field_weather_city'] = array(
    'bundle' => 'weather',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'bean',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_weather_city',
    'label' => 'City',
    'description' => 'City name and country (optional), such as: Boulder, CO',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'bean-weather-field_weather_units'
  $field_instances['bean-weather-field_weather_units'] = array(
    'bundle' => 'weather',
    'default_value' => array(
      0 => array(
        'value' => 'imperial',
      ),
    ),
    'default_value_function' => '',
    'default_value_php' => '',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'bean',
    'fences_wrapper' => 'no_wrapper',
    'field_name' => 'field_weather_units',
    'label' => 'Units',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('City');
  t('Units');

  return $field_instances;
}
