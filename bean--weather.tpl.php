<div class="weather-bean-block">
  <?php print render($content['icon']); ?>
  <div class="weather-bean-city"><?php print render($content['city']); ?></div>
  <div class="weather-bean-temp"><?php print render($content['temp']); ?></div>
  <div class="weather-bean-conditions">Conditions: <?php print render($content['conditions']); ?></div>
  <div class="weather-bean-wind">Wind: <?php print render($content['wind']); ?></div>
  <div class="weather-bean-wind">Humidity: <?php print render($content['humidity']); ?></div>
</div>