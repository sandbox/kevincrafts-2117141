<?php
/**
 * @file
 * weather_bean.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function weather_bean_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'weather';
  $bean_type->label = 'Weather';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['weather'] = $bean_type;

  return $export;
}
